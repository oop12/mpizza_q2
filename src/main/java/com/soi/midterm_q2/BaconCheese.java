/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.midterm_q2;

/**
 *
 * @author user
 */
public class BaconCheese extends Pizza {

    private int s = 89;
    private int m = 109;
    private int l = 119;

    public BaconCheese(String name, String topping, char size) {
        super(name, topping, size);
    }

    @Override
    public void printStatus() {
        System.out.println("Your order : " + this.name + "\n Topping : " + this.topping
                + " \n Size : " + this.size + " = " + price(size) + " Bath");
    }

    @Override
    public int price(char size) {
        int price = 0;
        switch (size) {
            case 'S':
            case 's':
                price = s;
                break;
            case 'M':
            case 'm':
                price = m;

                break;
            case 'L':
            case 'l':
                price = l;

                break;
            default:
                price = 0;
                break;
        }
        return price;

    }
}
