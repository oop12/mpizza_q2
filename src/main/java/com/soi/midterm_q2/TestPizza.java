/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.midterm_q2;

/**
 *
 * @author user
 */
public class TestPizza {

    public static void main(String[] args) {
        PizzaHouse();
        Pizza pizza = new Pizza("Unkonw", "None", 'n');
        pizza.printStatus();
        pizza.addCheese();
        pizza.total();

        PizzaHouse();
        BbqChicken pizza1 = new BbqChicken("Jane", "BBQ Chicken", 's');
        pizza1.printStatus();
        pizza1.addCheese(40);
        pizza1.total();

        PizzaHouse();
        BaconCheese pizza2 = new BaconCheese("Sara", "Bacon Cheese", 'm');
        pizza2.printStatus();
        pizza2.addCheese();
        pizza2.total();

        PizzaHouse();
        Sausage pizza3 = new Sausage("Jack", "Sausage", 'L');
        pizza3.printStatus();
        pizza3.addCheese(20);
        pizza3.total();

        PizzaHouse();
        BaconCheese pizza4 = new BaconCheese("John", "Bacon Cheese", 'L');
        pizza4.printStatus();
        pizza4.addCheese();
        pizza4.total();

        PizzaHouse();
        BbqChicken pizza5 = new BbqChicken("Bella", "BBQ Chicken", 'L');
        pizza5.printStatus();
        pizza5.addCheese(10);
        pizza5.total();

        System.out.println("+----------Polymorphism----------+");
        Pizza[] pizzas = {pizza1, pizza2, pizza3, pizza4, pizza5};
        for (int i = 0; i < pizzas.length; i++) {
            PizzaHouse();
            pizzas[i].printStatus();
            if (pizzas[i].cheakcheese()) {
                int num = 0;
                pizzas[i].addCheese(num);
            } else {
                pizzas[i].addCheese();
            }
            pizzas[i].total();
        }

    }

    private static void PizzaHouse() {
        System.out.println("-----PIZZA HOUSE-----");
    }

}
