/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.midterm_q2;

/**
 *
 * @author user
 */
public class Pizza {

    protected String name;
    protected String topping;
    protected char size;
    protected int s = 0;
    protected int m = 0;
    protected int l = 0;
    //ประกาศตัวแปร num ใช้ใน method addCheeseHowMuch(num) และ
    //ใช้ในการคำนวณใน method total()
    protected int num;
    //ประกาศตัวแปร c ใช้ใน method addCheese() และ method addCheeseHowMuch(num)
    protected int c = 0;

    public Pizza(String name, String topping, char size) {
        this.name = name;
        this.topping = topping;
        this.size = size;
        System.out.println("Pizza order");
    }

    public void printStatus() {
        System.out.println("Your order : " + this.name + "\n Topping : " + this.topping
                + " \n Size : " + this.size + " = " + price(size) + " Bath");
    }

    //ราคาต่อขนาด
    public int price(char size) {
        int price = 0;
        switch (size) {
            case 'S':
            case 's':
                price = s;
                break;
            case 'M':
            case 'm':
                price = m;

                break;
            case 'L':
            case 'l':
                price = l;

                break;
            default:
                price = 0;
                break;
        }
        return price;

    }

    public void total() {
        //บวกค่าเพิ่มชีส
        int total = price(size) + addCheese(num);
        System.out.println("Total : " + total + " Bath");

    }

    //ไม่เพิ่มชีส
    public void addCheese() {
        System.out.println(" Cheese : " + c + " Bath");

    }

    //เพิ่มชีส(เท่าไหร่)
    public int addCheese(int num) {
        if (num >= 10) {
            c = num;
            System.out.println(" Cheese : " + c + " Bath");
        }
        return c;
    }
    //เป็น method ที่เอาไว้ cheak ว่าเพิ่มชีสแบบเรียก method addCheese(num) มาหรือไม่
    //ซึ่ง method cheakcheese() เอาไว้ใช้ใน TestPizza ในตอนที่ทำ Polymorphism
    public boolean cheakcheese() {
        System.out.println(" Cheese : " + addCheese(num) + " Bath");
        return true;
    }

    public String getName() {
        return name;
    }

    public String getTopping() {
        return topping;
    }

    public char getSize() {
        return size;
    }

}
